class Name
{
    void name()
    {
        System.out.println("Name : lokesh");
    }
}
class Age extends Name
{
    void age()
    {
        System.out.println("Age : 20");
    }
}
class Main{
public static void main(String[] args)
{
    Age obj = new Age();
    obj.name();
    obj.age();
}
}