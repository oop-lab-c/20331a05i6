class Main
{
    private int privar;
    protected int provar;
    public int pubvar;
    public void setvar(int privalue,int provalue,int pubvalue)
    {
        privar = privalue;
        provar = provalue;
        pubvar = pubvalue;
    }
     void getvar()
    {
        System.out.println("the privalue is "+privar);
        System.out.println("the provalue is "+provar);
        System.out.println("the pubvalue is "+pubvar);
    }
    public static void main(String[] args)
    {
        Main b = new Main();
        b.setvar(24,50,56);
        b.getvar();
    }
}